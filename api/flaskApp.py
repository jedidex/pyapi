from flask import Flask

from controller import definition
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

definition.init_app(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
