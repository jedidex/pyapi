import math
from typing import List, Dict

from flask_restx.fields import Float
from neo4j import GraphDatabase, Result, Record
from neo4j.graph import Node, Relationship

from controller.model.core import SWElement
from mapper import YamlMapper
from utils.constants import NEO4J_PW, NEO4J_US, NEO4J_CS

uri = "neo4j://51.15.54.24:7687"
driver = GraphDatabase.driver(NEO4J_CS, auth=(NEO4J_US, NEO4J_PW))


def find_element(element_id: int, label: str = "SWElement"):
    def fetch_element(tx, ref: int):
        result: Result = tx.run(f"MATCH path=(n:{label} "
                                "{element_id:$ref})-[r]->(x) "
                                "OPTIONAL MATCH (n)-[r]->(x) "
                                "WHERE type(r) <> 'APPEARS_IN' and type(r) <> 'AFFILIATED_TO' "
                                "RETURN n, collect(relationships(path)) as r, collect(nodes(path)) as x",
                                ref=ref,
                                label=label)

        record: Record = result.single()

        if record is None:
            return None

        node: Node = record["n"]

        try:
            definition = YamlMapper.find_definition(node.labels)
        except LookupError:
            definition = None

        relationships: List[List[Relationship]] = record["r"]
        element = {}

        for key, value in node.items():
            if not key.startswith('_'):
                element[key] = value
            if key == "_wookiepedia_ref":
                element["wp"] = value

        # set the entity type
        temp_type: List[str] = list(node.labels)
        if len(temp_type) > 1:
            temp_type.remove("SWElement")
        element["entity"] = temp_type[0]

        # rename the id
        element["id"] = element["element_id"]
        del element["element_id"]

        if definition is not None:
            for i, rels in enumerate(relationships):
                rel = rels[0]  # always true since the path is only 1 relationship long
                destination = SWElement(rel.end_node)

                # find the relationship type
                field = definition.find_relationship_field(rel.type)
                if field is None:
                    continue

                if field.name not in element and field.single_element:
                    element[field.name] = destination.to_dict()

                elif not field.single_element:
                    if field.name not in element:
                        element[field.name] = []
                    element[field.name].append(destination.to_dict())

        return element

    with driver.session() as session:
        return session.read_transaction(fetch_element, element_id)


def find_appearances(element_id: int, label: str = "SWElement"):
    def fetch_appearances(tx, ref: int):
        result: Result = tx.run(
            f"MATCH path=(n:{label} "
            "{element_id:$ref})-[r:APPEARS_IN]-(x) "
            "RETURN x", ref=ref)

        results: List[Dict] = []

        for record in result:
            node: Node = record["x"]
            results.append(SWElement(node).to_dict())

        return results

    with driver.session() as session:
        return session.read_transaction(fetch_appearances, element_id)


def search_element(query: str,
                   size: int = 10,
                   page: int = 0,
                   label: str = "SWElement"):
    def fetch_appearances(tx):
        result: Result = tx.run(
            f"CALL db.index.fulltext.queryNodes(\"name\", \"{query}\") YIELD node, score where node:{label} RETURN node, score SKIP {size * page} LIMIT {size}")

        results: List[Dict] = []

        for record in result:
            node: Node = record["node"]
            score: Float = record["score"]

            element = SWElement(node).to_dict()
            element["searchScore"] = score

            results.append(element)

        count_result: Result = tx.run(
            f"CALL db.index.fulltext.queryNodes(\"name\",\"{query}\") YIELD node, score RETURN count(node) as count"
        )

        record: Record = count_result.single()
        count = int(record["count"])

        return {
            "page": page,
            "size": size,
            "totalElements": count,
            "totalPages": math.ceil(count / size),
            "results": results,
        }

    with driver.session() as session:
        return session.read_transaction(fetch_appearances)
