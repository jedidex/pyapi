import os
from enum import Enum
from typing import Dict, List

import yaml


class DataType(Enum):
    ELEMENT = "element"
    GENDER = "gender"
    NUMBER = "number"
    STRING = "string"
    LIST = "list"
    DATE = "date"
    DATE_RANGE = "date_range"
    IMAGE = "image"


class RelationDirection(Enum):
    OUT = "OUT"
    # IN = "IN"  # for now not supported! The problem reseeds in updating the relationships


class FieldDefinition:

    def __init__(self, name: str, dictionary: Dict):
        self.name: str = name

        # check if the dictionary has the required fields
        keys = dictionary.keys()

        if "type" not in keys or "template_name" not in keys:
            raise ImportError("Missing base keys template or fields")

        matches = [e for e in DataType if e.value == dictionary["type"]]
        if len(matches) != 1:
            unknown = dictionary["type"]
            raise ImportError(f"Unknown data type '{unknown}'")

        self.data_type = matches[0]
        self.template_key = dictionary["template_name"]

        # check if is an element type
        if self.data_type is DataType.ELEMENT:
            # check if the dictionary has the required fields
            if "rel" not in keys or "direction" not in keys:
                raise ImportError("Missing relation fields")

            given_relation_direction = dictionary["direction"]
            matches = [e for e in RelationDirection if e.value == given_relation_direction]
            if len(matches) != 1:
                unknown = given_relation_direction
                raise ImportError(f"Unknown relation type '{unknown}'")

            self.relation_direction = matches[0]
            self.relation_name = dictionary["rel"]
            if "single_element" in dictionary:
                self.single_element = dictionary["single_element"]
            else:
                self.single_element = False


class ElementDefinition:

    def __init__(self, dictionary: Dict):

        # check if the dictionary has the required fields
        keys = dictionary.keys()

        if "template" not in keys or "fields" not in keys or "is_media" not in keys or "label" not in keys:
            raise ImportError("Missing base keys template or fields or is_media or label")

        self.supported_templates: List[str] = [x.replace(" ", "_").lower() for x in dictionary["template"]]
        self.fields: List[FieldDefinition] = []

        self.is_media = dictionary["is_media"]
        self.label: str = dictionary["label"]

        # populate the fields
        fields_dict = dictionary["fields"]
        for name, value in fields_dict.items():
            self.fields.append(FieldDefinition(name, value))

    def find_relationship_field(self, relation_name: str):
        matches = [x for x in self.fields if x.data_type == DataType.ELEMENT and x.relation_name == relation_name]
        if len(matches) == 1:
            return matches[0]
        return None


# search for all the yaml files
yaml_files = []
for root, dirs, files in os.walk(os.environ.get("RESOURCES_PATH", "../resources")):
    for file in files:
        if file.endswith(".yml"):
            yaml_files.append(os.path.join(root, file))

# deserialize the yaml
definitions = []
for path in yaml_files:
    with open(path, "r") as stream:
        try:
            definitions.append(yaml.safe_load(stream))
        except yaml.YAMLError as exc:
            print(exc)

definitions = [ElementDefinition(x) for x in definitions]
t = [x.supported_templates for x in definitions]
supported_templates = [item for sublist in t for item in sublist]

if len(supported_templates) != len(set(supported_templates)):
    raise RuntimeError("Found ambiguous set of definitions, check the yaml files for duplicated template")


def find_definition(label: List[str]) -> ElementDefinition:
    matches = [x for x in definitions if x.label in label]

    if len(matches) > 1:
        raise RuntimeError("Ambiguous set of definitions found")
    elif len(matches) == 0:
        raise LookupError("No definitions found")

    return matches[0]
