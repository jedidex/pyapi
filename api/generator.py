from string import Template
import re

from mapper import YamlMapper

imports = []

with open('controller/elements/template', 'r') as f:
    src = Template(f.read())

    for definition in YamlMapper.definitions:
        label = definition.label.replace("-", "")
        plural_label = f"{label}s"  # yes... I know
        namespace = plural_label.lower()

        # small check to avoid t-v-series case
        if label.startswith("TV"):
            label.replace("TV", "Tv")

        url_name = re.sub(r'(?<!^)(?=[A-Z])', '-', label).lower() + "s"

        imports.append(
            {
                "file": label,
                "ns": namespace
            }
        )

        d = {
            'label_plural_lc': namespace,
            'label_plural': plural_label,
            'label': label,
            'url_name': url_name,
            'appearances': "appearances" if definition.is_media else "appears_in"
        }

        result = src.substitute(d)

        with open(f"controller/elements/{label}.py", "wt") as py:
            py.write(result)

with open('controller/template', 'r') as f:
    src = Template(f.read())

    d = {
        'imports': '\n'.join([f"from controller.elements.{x['file']} import {x['ns']}_ns" for x in imports]),
        'namespaces': '\n'.join([f"definition.add_namespace({x['ns']}_ns)" for x in imports]),
    }

    result = src.substitute(d)

    with open(f"controller/__init__.py", "wt") as py:
        py.write(result)
