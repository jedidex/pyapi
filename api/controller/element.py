from flask_restx import Namespace, Resource, reqparse, abort

from repository import ElementRepository

elements_ns = Namespace('elements', description='Elements')


@elements_ns.route('/<identifier>')
class Element(Resource):

    @elements_ns.doc(description='Fetch a single element')
    def get(self, identifier: str):
        element = ElementRepository.find_element(int(identifier))
        if element is None:
            abort(404, "Requested resource not found")
        return element


@elements_ns.route('/search')
class Search(Resource):

    @elements_ns.doc(description='Perform a search',
                     params={
                         'q': {
                             'description': 'Search term',
                             'type': 'str'},
                         'size': {
                             'description': 'Page size',
                             'type': 'int',
                             'default': 10},
                         'page': {
                             'description': 'Page number',
                             'type': 'int',
                             'default': 0},

                     },
                     )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('q', type=str, required=True)
        parser.add_argument('page', type=int, default=0)
        parser.add_argument('size', type=int, default=10)
        args = parser.parse_args()

        return ElementRepository.search_element(args["q"], args["size"], args["page"])
