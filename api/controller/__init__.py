from flask_restx import Api

from controller import elements
from controller.element import elements_ns

definition = Api(version='0.1',
                 title='Jedidex API',
                 description='Largest set of StarWars API of the internet',
                 doc='/docs/')

# add the namespace to the api definition
definition.add_namespace(elements_ns)
