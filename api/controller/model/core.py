from typing import List

from neo4j.graph import Node


class SWElement:
    def __init__(self, node: Node):
        self.image = node.get("image", None)
        self.name = node.get("name", None)
        self.id = node.get("element_id", None)
        self.wp = node.get("_wookiepedia_ref", None)
        temp_type: List[str] = list(node.labels)
        if len(temp_type) > 1:
            temp_type.remove("SWElement")
        self.type = temp_type[0]

    def to_dict(self):
        return {
            "image": self.image,
            "name": self.name,
            "wp": self.wp,
            "entity": self.type,
            "id": self.id
        }
