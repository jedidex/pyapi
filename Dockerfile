FROM python:3.7.9-slim-buster
#RUN apt-get update && \
#    apt-get install -y software-properties-common gnupg

#RUN add-apt-repository ppa:sdurobotics/ur-rtde
#RUN apt-get update && apt install librtde librtde-dev -y

ENV WORKERS="3"
ENV THREADS="5"
ENV RESOURCES_PATH="/yamls"

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY api ./
COPY resources ./yamls
RUN chmod +x wsgi.py

RUN python generator.py

EXPOSE 5000
CMD gunicorn --bind 0.0.0.0:5000 --threads=$THREADS --workers=$WORKERS wsgi:app
